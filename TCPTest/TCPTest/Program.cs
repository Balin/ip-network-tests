﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System;

namespace TCPServerTest
{
	class MainClass{

		class ConnectionInstance{
			public Socket socket;
			public byte[] receiveBuffer = new byte[256];
			public List<byte> receivedStream = new List<byte>();
			public List<byte> sendStream = new List<byte>();
			public Thread thread = null;
			public bool disconnected = false;
			public int lastMessageSent;

			public ConnectionInstance(Socket newConnection, int lastMessage){	socket = newConnection;	lastMessageSent = lastMessage;	}
		}

		static readonly string ln = System.Environment.NewLine;

		private static bool readingMessages = false;
		private static List<byte[]> messageList = new List<byte[]>();
		private static int lastMessage = -1;
		private static Socket acceptSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		private static List<ConnectionInstance> connectionInstances = new List<ConnectionInstance>();

		public static void Main(string[] args){
			if(args.Length == 0) {	WrongParameters();	return;	}

			int bindPort = int.Parse(args[0]);

			acceptSocket.Bind(new IPEndPoint(IPAddress.Any,bindPort));
			acceptSocket.Listen(16);
			while(true){
				while(acceptSocket.Poll(50,SelectMode.SelectRead)){
					ConnectionInstance newInstance = new ConnectionInstance(acceptSocket.Accept(), lastMessage);
					Console.WriteLine("Client Connected: " +newInstance.socket.RemoteEndPoint);
					newInstance.socket.SendTimeout = 1000;
					newInstance.socket.ReceiveTimeout = 1000;
					connectionInstances.Add(newInstance);
					NetThread threadInstance = new NetThread(newInstance);
					newInstance.thread = new Thread(new ThreadStart(threadInstance.Work));
					newInstance.thread.Start();
				}
				connectionInstances.RemoveAll(ConnectionInstance => ConnectionInstance.disconnected == true);
			}
		}

		private class NetThread{
			ConnectionInstance instance;

			public NetThread(ConnectionInstance instance){
				this.instance = instance;
			}

			public void Work(){
				while (true){
					int receivedByteCount = instance.socket.Available;
					if (instance.socket.Poll(2, SelectMode.SelectRead) && receivedByteCount == 0) {
						Console.WriteLine("Client Disconnected: "+instance.socket.RemoteEndPoint);
						break;
					}

					if (instance.socket.Poll(2,SelectMode.SelectRead) && receivedByteCount > 0){
						int receivedBytes = instance.socket.Receive(instance.receiveBuffer);
						instance.receivedStream.Clear();
						for (int i = 0; i < receivedByteCount; i++) { instance.receivedStream.Add(instance.receiveBuffer[i]); }
						Console.WriteLine(
							"Received message from: "+instance.socket.RemoteEndPoint+ln
							+Encoding.UTF8.GetString(instance.receivedStream.ToArray())
						);

						while(readingMessages){	Thread.Sleep(0);	}
						readingMessages = true;
						messageList.Add(instance.receivedStream.ToArray());
						lastMessage++;
						instance.lastMessageSent++;
						readingMessages = false;

						int sentBytes2 = instance.socket.Send(instance.receivedStream.ToArray());
					}
					if(instance.lastMessageSent < lastMessage) {
						while(readingMessages){	Thread.Sleep(0);	}
						readingMessages = true;
						instance.socket.Send(messageList[lastMessage]);
						instance.lastMessageSent++;
						readingMessages = false;
					}

					Thread.Sleep(250);
				}
				instance.socket.Shutdown(SocketShutdown.Both);
				instance.socket.Close();
				instance.disconnected = true;
			}
		}

		static void WrongParameters() {
			Console.Write("Usage: programName.exe (port number)" + ln + "ex: TCPServerTest.exe 6789");
		}
	}
}
