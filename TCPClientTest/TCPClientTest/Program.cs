﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System;

namespace TCPClientTest {
	class MainClass {
		static readonly string ln = System.Environment.NewLine;

		static Socket tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		static Thread networkThread;
		static string sendString = "C#TEST123";
		static int threadSleep = 100;

		static IPAddress targetIp;
		static int bindPort;

		public static void Main(string[] args) {
			if(args.Length == 0) { WrongParameters(); return; }

			string[] ipAddressStrings = args[0].Split(new char[] { '.' });
			if(ipAddressStrings.Length != 4) { WrongParameters(); return; }

			byte[] ipAddressBytes = new byte[4];
			for(int i = 0; i < 4; i++) { ipAddressBytes[i] = byte.Parse(ipAddressStrings[i]); }
			targetIp = new IPAddress(ipAddressBytes);

			bindPort = int.Parse(args[1]);

			networkThread = new Thread(StartNetwork);
			networkThread.Start();
		}

		static void StartNetwork() {
			tcpSocket.Connect(targetIp, bindPort);
			Console.WriteLine("Connected to: "+tcpSocket.RemoteEndPoint);

			byte[] sendBytes = Encoding.UTF8.GetBytes(sendString);
			int sentBytes = tcpSocket.Send(sendBytes);
			//sendDone.WaitOne();

			while(true) {
				int receivedByteCount = tcpSocket.Available;
				if(tcpSocket.Poll(10, SelectMode.SelectRead) && receivedByteCount == 0) {
					Console.WriteLine("Disconnected from: "+tcpSocket.RemoteEndPoint);
					break;
				}
				if(tcpSocket.Poll(10, SelectMode.SelectRead) && receivedByteCount > 0) {
					byte[] receivedArray = new byte[256];
					List<byte> receivedList = new List<byte>();
					int receivedBytes = tcpSocket.Receive(receivedArray);
					receivedList.Clear();
					for(int i = 0; i < receivedBytes; i++) { receivedList.Add(receivedArray[i]); }
					Console.WriteLine(
						"Received message from: "+tcpSocket.RemoteEndPoint+ln+
						Encoding.UTF8.GetString(receivedList.ToArray())
					);
					int sentBytesReply = tcpSocket.Send(receivedList.ToArray());
				}
				Thread.Sleep(threadSleep);
			}
		}

		static void WrongParameters() {
			Console.Write("Usage: programName.exe (remote IP address) (port number)" + ln + "ex: TCPClientTest.exe 127.0.0.1 6789");
		}
	}
}
