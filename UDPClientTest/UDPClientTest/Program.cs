﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;

namespace UDPClientTest {
	class MainClass {
		static Socket listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
		static readonly string ln = System.Environment.NewLine;

		public static void Main(string[] args) {
			if(args.Length < 2) {	WrongParameters();	return; }

			string[] ipAddressStrings = args[0].Split(new char[] { '.' });
			if(ipAddressStrings.Length != 4) {	WrongParameters();	return;	}

			byte[] ipAddressBytes = new byte[4];
			for(int i = 0; i < 4; i++){ ipAddressBytes[i] = byte.Parse(ipAddressStrings[i]);	}
			IPAddress targetIp = new IPAddress(ipAddressBytes);

			int bindPort = int.Parse(args[1]);

			listeningSocket.Bind(new IPEndPoint(IPAddress.Any, 6789));
			byte[] sendBytes = Encoding.UTF8.GetBytes("C#TEST");
			listeningSocket.SendTo(sendBytes, 0, (EndPoint)new IPEndPoint(targetIp, bindPort));
			while(true) {
				if(listeningSocket.Poll(10, SelectMode.SelectRead) && listeningSocket.Available > 0) {
					byte[] receiveBuffer = new byte[256];
					EndPoint sender = new IPEndPoint(IPAddress.Any, bindPort);
					int receivedBytesCount = listeningSocket.ReceiveFrom(receiveBuffer, ref sender);
					List<byte> message = new List<byte>();
					for(int i = 0; i < receivedBytesCount; i++) { message.Add(receiveBuffer[i]); }
					Console.WriteLine(
						"Received message from: " + ((IPEndPoint)sender).Address + ln
						+ Encoding.UTF8.GetString(message.ToArray())
					);
					listeningSocket.SendTo(message.ToArray(), 0, sender);
				}
			}
		}

		static void WrongParameters(){
			Console.Write("Usage: programName.exe (remote IP address) (port number)" + ln + "ex: UDPClientTest.exe 127.0.0.1 6789");
		}
	}
}
