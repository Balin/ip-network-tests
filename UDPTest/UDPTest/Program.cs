﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;

namespace UDPServerTest {
	class MainClass {
		static Socket listeningSocket = new Socket(AddressFamily.InterNetwork,SocketType.Dgram,ProtocolType.Udp);
		static readonly string ln = System.Environment.NewLine;

		public static void Main(string[] args) {
			if(args.Length == 0){	WrongParameters(); return;	}

			int bindPort = int.Parse(args[0]);
			listeningSocket.Bind(new IPEndPoint(IPAddress.Any,bindPort));
			while(true) {
				if(listeningSocket.Poll(10, SelectMode.SelectRead) && listeningSocket.Available > 0) {
					byte[] receiveBuffer = new byte[256];
					EndPoint sender = new IPEndPoint(IPAddress.Any,6789);
					int receivedBytesCount = listeningSocket.ReceiveFrom(receiveBuffer, ref sender);
					List<byte> message = new List<byte>();
					for(int i = 0; i < receivedBytesCount; i++){	message.Add(receiveBuffer[i]);	}
					Console.WriteLine(
						"Received message from: "+((IPEndPoint)sender).Address+ln
						+Encoding.UTF8.GetString(message.ToArray())
					);
					listeningSocket.SendTo(message.ToArray(),0,sender);
				}
			}
		}

		static void WrongParameters() {
			Console.Write("Usage: programName.exe (port number)" + ln + "ex: UDPServerTest.exe 6789");
		}
	}
}
